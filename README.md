# btflix-login


## About Project
This project was created in NodeJs to be used in the registration and login area of the btflix video platform.


## Getting started
To start the project, run the following command:

* npm install

## Dependencies
* ExpressJS - Framework that facilitates the construction of web applications and api.
* Body-parser - Help ExpressJS to capture body in post type request.
* Dotenv - To load enviroments variables.
* Jsonwebtoken - To generate jwt token.
* Mongodb - Nosql to save login data.
* Mongodb-memory-server - To create mock nosql database.
* Nodemon - To speed up the development restarting the server automatically.
* Supertest- For integrated tests using http requests.