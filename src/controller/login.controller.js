const jwt = require('jsonwebtoken');
const loginService = require('../service/login.service');


const signin = async function(req,res){
    const login = await loginService.signin(req.query)
    if(login !== null && login !== undefined){
        const token = jwt.sign(login, process.env.SECRET, {expiresIn: 300})
        return res.status(200).json({auth: true, token:token})
    }
    return res.status(500).json({message: 'The email or password is invalid'})
}


const register = async function(req,res){
    try{
        await loginService.register(req.body)
        return res.status(201).json({message: 'create'})
    }catch(error){
        return res.status(500).json({message: 'Error to register user'})
    }
}

module.exports = {
    signin,
    register
}