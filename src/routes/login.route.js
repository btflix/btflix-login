const express = require('express');

const router = express.Router({mergeParams: true});

const loginController = require('../controller/login.controller')


router.route('/signin').get(loginController.signin)

router.route('/register').post(loginController.register)

module.exports = router;