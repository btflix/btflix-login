const express = require('express');
bodyParser = require('body-parser').json();

const login = require('./login.route');

const router = express.Router();


router.use('/login', bodyParser, login);

module.exports = router;