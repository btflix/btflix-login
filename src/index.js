require('dotenv').config()
const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors())

const route = require("./routes/index.route")
app.use(route)


if(require.main === module){
    const server = app.listen(8080, function(){
        let host = server.address().address
        let port = server.address().port

        console.log("Initalized the app at http://%s:%s", host, port)
    })
}

module.exports = app