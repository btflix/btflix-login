const db = require('../database/user.db')


const signin = async function(param){
    return await db.findUser(param)
}

const register = async function(param){
    if(Object.keys(param).length === 0){
        throw new Error("Not expected object value empty")
    }
    return await db.register(param)
}

module.exports = {
    signin,
    register
}