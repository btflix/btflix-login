const clientDatabase = require("./db")

const collectionUser = "user";

async function findUser(param){
    return await clientDatabase.findOne(collectionUser, param)
}
  
  
async function register(param){  
    const object = {
        name: param.name,
        document: param.document,
        address: param.address,
        number: param.number,
        phone: param.phone,
        email: param.email,
        password: param.password
    }
    return await clientDatabase.insertOne(collectionUser, object);
}



  
  module.exports = {
    findUser,
    register
  }