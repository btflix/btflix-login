const { MongoClient } = require("mongodb");
const { MongoMemoryServer } = require('mongodb-memory-server');

let client;

async function connect() {
      let dbUrl = process.env.MONGO_HOST;

      if(process.env.NODE_ENV === 'test'){
            mongod = await MongoMemoryServer.create();
            dbUrl = mongod.getUri();
      }
      
      client = new MongoClient(dbUrl);
      
      return await client.connect();

}

async function getCollection(collectionName){
      myConnect = await connect();
      const db = myConnect.db(process.env.MONGO_DATABASE)
      return db.collection(collectionName)
}

async function findOne(collectionName, object){
      collection = await getCollection(collectionName)
      if(process.env.NODE_ENV === 'test' && Object.keys(object).length !== 0){
            await collection.insertOne(object);
      }
      return await collection.findOne(object)
}

async function insertOne(collectionName, object){
      collection = await getCollection(collectionName)
      return await collection.insertOne(object)
}


module.exports = {findOne, insertOne}
