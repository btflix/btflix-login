const userDb = require("../../src/database/user.db")

test('create user', async () => {
    const payload = 
        {
            name: 'bryan',
            document: '143.143.143-43',
            address: 'rua blaze',
            number: '182',
            phone: '21 989877442',
            email: 'bryan@teste.com',
            password: '1234'
        }

    const result = await userDb.register(payload);
    
    expect(result.acknowledged).toEqual(true);
})

test('find user', async () => {
    const payload = 
    {
        email: 'bryan@teste.com',
        password: '1234'
    }


    await userDb.register(payload);
    const result = await userDb.findUser(payload)
    expect(result.email).toEqual(payload.email)
})



test('user not found', async () => {
    const payload = 
    {
        email: 'bryan@teste.com',
        password: '1234'
    }


    await userDb.register(payload);
    const result = await userDb.findUser({})
    expect(result).toEqual(null)
})