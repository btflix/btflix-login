const supertest = require('supertest')
const index = require('../../src/index')
const loginController = require('../../src/controller/login.controller');

test('test success register login', async() => {

    const response = await supertest(index)
        .post('/login/register')
        .send(getPayload())
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        
    expect(response.status).toEqual(201);
})


test('test error register login', async() => {

    const response = await supertest(index)
        .post('/login/register')
        .send({})
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        
    expect(response.status).toEqual(500);
})

test('test signin login', async() => {
    

    loginController.signin
    const response = await supertest(index)
        .get('/login/signin')
        .query(getPayload())
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')

    expect(response.status).toEqual(200);
})


test('test error signin login', async() => {
    

    loginController.signin
    const response = await supertest(index)
        .get('/login/signin')
        .query({})
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')

    expect(response.status).toEqual(500);
})


function getPayload(){
    return {
        name: 'bryan',
        document: '143.143.143-43',
        address: 'rua blaze',
        number: '182',
        phone: '21 989877442',
        email: 'bryan@teste.com',
        password: '1234'
    }
}